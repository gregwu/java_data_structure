package juc;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @Author linhao
 * @Date created in 4:04 下午 2021/3/28
 */
public class Student implements Comparable<Student>{

    private String name;
    private int age;

    /**
     * 小于0则后置，大与0则前置
     * @param obj
     * @return
     */
    @Override
    public int compareTo(Student obj) {
        return 1;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
//        List<Student> studentList = new ArrayList<>();
        PriorityBlockingQueue<Student> priorityBlockingQueue = new PriorityBlockingQueue();
        for(int i=0;i<10;i++){
//            Student student = new Student("idea",i+10);
//            studentList.add(student);
            priorityBlockingQueue.add(new Student("idea",i+10));
        }
        Iterator<Student> iterator = priorityBlockingQueue.iterator();
        while (iterator.hasNext()){
            Student student = iterator.next();
            System.out.println(student);
        }
//        Collections.sort(studentList);
//        studentList.stream().forEach(e -> System.out.println(e));


    }
}
