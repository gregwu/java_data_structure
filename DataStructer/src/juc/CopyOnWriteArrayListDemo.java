package juc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * copyonwritearraylist的读性能要好过vector,写性能稍差一些
 * @Author linhao
 * @Date created in 9:29 上午 2021/3/28
 */
public class CopyOnWriteArrayListDemo {

    public static void main(String[] args) {
        //构造函数不支持初始化值大小
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for(int i=100;i<200;i++){
            copyOnWriteArrayList.add(i);
        }
        System.out.println(copyOnWriteArrayList.contains(-1));
        //读取数据的时候底层比较简单，没有加锁之类的操作
        copyOnWriteArrayList.get(0);
        //修改数据的时候加入了lock锁，并且对于底层数组进行了拷贝操作
        copyOnWriteArrayList.add(1);
        System.out.println(copyOnWriteArrayList.lastIndexOf(-1));

        List<String> list = Collections.synchronizedList(new ArrayList<>(10));
        for(int i=0;i<100;i++){
            list.add("item"+i);
        }
        System.out.println(list.size());
    }
}
