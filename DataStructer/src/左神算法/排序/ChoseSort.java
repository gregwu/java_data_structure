package 左神算法.排序;

/**
 * 选择排序
 * 最简单但是最没用的排序算法
 * 工程中基本不用
 *
 * @Author linhao
 * @Date created in 8:54 上午 2022/1/25
 */
public class ChoseSort {

    public static int[] arr = {12, 34, 122, 523, 6,6, 34};


    public static void sort(int[] arr){
        for(int i=0;i<arr.length;i++){
            findMin(arr,i);
        }
    }

    public static void findMin(int[] arr, int beginIndex) {
        int minIndex = beginIndex;
        for (int j = beginIndex; j < arr.length; j++) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
        swap(arr,beginIndex,minIndex);
    }

    public static void displayArr(){
        for (int i : arr) {
            System.out.print(" "+i);
        }
    }

    public static void swap(int[] arr, int j, int p) {
        int temp = arr[j];
        arr[j] = arr[p];
        arr[p] = temp;
    }

    public static void main(String[] args) {
        ChoseSort.sort(arr);
        ChoseSort.displayArr();
    }
}
