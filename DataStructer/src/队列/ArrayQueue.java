package 队列;

/**
 * 数组队列
 *
 * @author idea
 * @data 2018/10/25
 */
public class ArrayQueue {

    private String[] items;

    private int capacity = 0;

    private int head = 0;

    private int tail = 0;

    private int size=0;

    public ArrayQueue(int size) {
        items = new String[size];
        capacity = size;
    }

    /**
     * 入队 存在问题 如果先删除元素后边再次入队的时候顺序队列里面会有多余空间，导致空间浪费
     *
     * @param value
     * @return
     */
    public boolean enqueue(String value) {
        //无法添加多余元素
        if (size == capacity) {
            return false;
        }
        adjustSize();
        items[tail] = value;
        tail++;
        size++;
        return true;
    }

    /**
     * 自动调整空间
     */
    private void adjustSize() {
        if (tail <= capacity && head < tail && head > 0) {
            int i,j;
            int s=0;
            for (i = 0, j = head; i < (tail - head ); i++, j++) {
                items[i] = items[j];
                items[j]=null;
                s++;
            }
            head=0;
            tail=s;
        }
    }

    /**
     * 出队
     *
     * @return
     */
    public String dequeue() {
        if (size == 0) {
            return null;
        }
        String value = items[head];
        items[head] = null;
        head++;
        size--;
        return value;
    }

    /**
     * 打印相应的内容
     */
    public void display() {
        for (int i = 0; i < capacity; i++) {
            System.out.print(items[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ArrayQueue aq = new ArrayQueue(8);
        //先存储5个元素，出队3个元素
        aq.enqueue("1");
        aq.enqueue("2");
        aq.enqueue("3");
        aq.enqueue("4");
        aq.enqueue("5");
        aq.display();

        String i1 = aq.dequeue();
        String i2 = aq.dequeue();
        String i3 = aq.dequeue();
//        System.out.println("this is :" + i1 + ":" + i2 + ":" + i3);
        aq.display();
        aq.enqueue("6");
        aq.enqueue("7");
        aq.enqueue("8");
        aq.enqueue("9");
        aq.display();
        aq.enqueue("10");
        aq.enqueue("11");
        aq.display();
        aq.dequeue();
        aq.dequeue();
        aq.display();
        aq.enqueue("12");
        aq.display();
    }

}
