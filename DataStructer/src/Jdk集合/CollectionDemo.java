package Jdk集合;


import org.omg.CORBA.TIMEOUT;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 研究Collection内部各个模块的细节
 *
 * @Author linhao
 * @Date created in 10:20 上午 2022/1/23
 */
public class CollectionDemo {

    public void disPlayArrayList() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add(2);
        Collections.binarySearch(arrayList, 8);
        ArrayList arrayList1 = new ArrayList(4);
        ArrayList arrayList2 = new ArrayList(arrayList);
    }

    public void displayVector() {
        Vector vector = new Vector();
        vector.add(1);
        vector.add(2);
    }

    /**
     * 按照key的插入顺序进行排序
     */
    public void displayLinkedListHashMap() {
        Map<String, Object> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("item1", 1);
        linkedHashMap.put("item22", 2);
        linkedHashMap.put("item32", 3);
        linkedHashMap.put("item41", 4);
        linkedHashMap.put("item55", 5);
        linkedHashMap.put("item63", 6);
        for (String key : linkedHashMap.keySet()) {
            System.out.println(linkedHashMap.get(key));
        }
    }


    /**
     * 可以支持根据传入的key进行大小排序
     */
    public void displayTreeMap() {
        Map<Integer, Object> treeMap = new TreeMap<>();
        treeMap.put(1, "1");
        treeMap.put(78, "2");
        treeMap.put(123, "3");
        treeMap.put(5444, "4");
        Set<Integer> keys = treeMap.keySet();
        for (Integer key : keys) {
            System.out.println("key is:" + key + " value is :" + treeMap.get(key));
        }
    }

    public void displayLinkedList() {
        LinkedList linkedList1 = new LinkedList();
        linkedList1.add(122);
        linkedList1.add(21);
        linkedList1.add(333);
        linkedList1.add(41);
        System.out.println(linkedList1.get(3));
    }


    public void testSet(){
        //无顺序性
        HashSet hashSet = new HashSet();
        hashSet.add(234);
        hashSet.add(12);
        hashSet.add(32);

        for (Object obj : hashSet) {
            System.out.println(obj);
        }

        System.out.println("==========");
        //按照插入的顺序会被记录到一个额外的list中，遍历时候满足fifo
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(234);
        linkedHashSet.add(12);
        linkedHashSet.add(32);

        //按照插入元素的大小进行排序
        TreeSet ts = new TreeSet();
        ts.add(234);
        ts.add(12);
        ts.add(21);
        for (Object obj : ts) {
            System.out.println(obj);
        }

        System.out.println("========");

        for (Object obj : linkedHashSet) {
            System.out.println(obj);
        }
    }

    public void testArrayBlockingQueue() throws InterruptedException {
        ArrayBlockingQueue arrayBlockingQueue = new ArrayBlockingQueue(1);
        System.out.println("take操作");
        arrayBlockingQueue.take();
        System.out.println("poll操作");
        Object val = arrayBlockingQueue.poll(2,TimeUnit.SECONDS);
        System.out.println(val);
        arrayBlockingQueue.add(1);
        arrayBlockingQueue.add(2);
        arrayBlockingQueue.add(3);
    }

    public void testQueue(){
        //单端队列
        ArrayDeque queue = new ArrayDeque();
        queue.add(811);
        queue.add(21);
        queue.add(233);
        queue.add(51);
        queue.remove(51);
        queue.remove(811);
        //按照fifo顺序去存储元素
        for (Object val : queue) {
            System.out.println(val);
        }
    }

    class Task{
        int level;
        String taskName;

        public Task(int level, String taskName) {
            this.level = level;
            this.taskName = taskName;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getTaskName() {
            return taskName;
        }

        public void setTaskName(String taskName) {
            this.taskName = taskName;
        }

        @Override
        public String toString() {
            return "Task{" +
                    "level=" + level +
                    ", taskName='" + taskName + '\'' +
                    '}';
        }
    }

    public void testPriorityBlockingQueue(){
        //可以支持按照自定义的优先级进行排序
        PriorityQueue<Task> priorityQueue = new PriorityQueue<>(new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getLevel()-o2.getLevel();
            }
        });
        priorityQueue.add(new Task(11,"任务1"));
        priorityQueue.add(new Task(42,"任务2"));
        priorityQueue.add(new Task(3,"任务3"));
        while (true){
            Task t = priorityQueue.poll();
            System.out.println(t.toString());
        }
    }





    public static void main(String[] args) throws InterruptedException {
        CollectionDemo collectionDemo = new CollectionDemo();
        collectionDemo.testPriorityBlockingQueue();
//        collectionDemo.displayTreeMap();
    }
}
