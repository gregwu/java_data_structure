package 算法复习课.手动造轮子.list;

import java.util.Iterator;

/**
 * @author linhao
 * @date created in 10:24 下午 2020/10/19
 */
public interface List<T> {

    T add(T t);

    void set(int index,T data);

    T remove(int index);

    int indexOf(T data);

    boolean contain(T data);

    T get(int index);

    void grow();

    boolean isEmpty();

    void display();

    Iterator iterator();

    /**
     * 基于fast-fail机制来处理多个线程同时使用迭代器访问同一个list容器时候可能会有的线程安全异常问题
     *
     * @return
     */
    boolean ensureListSafe();
}
